from flask_sqlalchemy import SQLAlchemy
from sqlalchemy_utils import ChoiceType

db = SQLAlchemy()

entries_groups = db.Table('entries_groups',
                          db.Column('entry_id', db.Integer(), db.ForeignKey('entry.id')),
                          db.Column('group_id', db.Integer(), db.ForeignKey('group.id')))

entries_components = db.Table('entries_components',
                          db.Column('entry_id', db.Integer(), db.ForeignKey('entry.id'), primary_key=True),
                          db.Column('component_of_id', db.Integer(), db.ForeignKey('entry.id')), primary_key=True)


class Group(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.Unicode)


class Entry(db.Model):
    ENTRY_TYPE_WORD = u"WORD"
    ENTRY_TYPE_PHRASE = u"PHRASE"

    ENTRY_TYPES = [
        (ENTRY_TYPE_WORD, 'word',),
        (ENTRY_TYPE_PHRASE, 'phrase',),
    ]

    # A noun is the name of a person, place, thing, or idea.
    # man... Butte College... house... happiness
    #   - gender: masculine, feminine, neuter
    #   - case: nominative, accusative, dative, genitive
    #   - number: singular, plural
    POS_TYPE_NOUN = u"NOUN"

    # A verb expresses action or being.
    # jump... is... write... become
    #   - person: 1st, 2nd, 3rd
    #   - number: singular, plural
    #   - tense: present, past, perfect, past perfect, future, future perfect
    #   - mood: indicative, conditional, imperative, subjunctive
    POS_TYPE_VERB = u"VERB"

    # A pronoun is a word used in place of a noun.
    # She... we... they... it
    POS_TYPE_PRONOUN = u"PRONOUN"

    # An adjective modifies or describes a noun or pronoun.
    # pretty... old... blue... smart
    POS_TYPE_ADJECTIVE = u"ADJECTIVE"

    # An adverb modifies or describes a verb, an adjective, or another adverb.
    # gently... extremely... carefully... well
    POS_TYPE_ADVERB = u"ADVERB"

    # A preposition is a word placed before a noun or pronoun to form a
    #   phrase modifying another word in the sentence.
    # by... with.... about... until
    POS_TYPE_PREPOSITION = u"PREPOSITION"

    # A conjunction joins words, phrases, or clauses.
    # and... but... or... while... because
    POS_TYPE_CONJUNCTION = u"CONJUNCTION"

    # An interjection is a word used to express emotion.
    # Oh!... Wow!... Oops!
    POS_TYPE_INTERJECTION = u"INTERJECTION"

    POS_TYPES = [
        (POS_TYPE_NOUN, 'noun',),
        (POS_TYPE_VERB, 'verb',),
        (POS_TYPE_PRONOUN, 'pronoun',),
        (POS_TYPE_ADJECTIVE, 'adjective',),
        (POS_TYPE_ADVERB, 'adverb',),
        (POS_TYPE_PREPOSITION, 'preposition',),
        (POS_TYPE_CONJUNCTION, 'conjunction',),
        (POS_TYPE_INTERJECTION, 'interjection',),
    ]

    id = db.Column(db.Integer, primary_key=True)

    date = db.Column(db.DateTime)

    english = db.Column(db.Unicode)
    german = db.Column(db.Unicode)

    english_audio_filepath = db.Column(db.Unicode)
    german_audio_filepath = db.Column(db.Unicode)

    entry_type = db.Column(ChoiceType(ENTRY_TYPES))

    pos = db.Column(ChoiceType(POS_TYPES))

    variant_of_id = db.Column(db.Integer, db.ForeignKey('entry.id'))
    variant_of = db.relationship("Entry",
                                 remote_side='Entry.id',
                                 backref="variants",
                                 foreign_keys=[variant_of_id])

    groups = db.relationship('Group',
                             secondary=entries_groups,
                             backref=db.backref('entries', lazy='dynamic'))

    components = db.relationship('Entry',
                                 secondary=entries_components,
                                 foreign_keys=[entries_components.c.component_of_id],
                                 primaryjoin="Entry.id == foreign(entries_components.c.entry_id)",
                                 secondaryjoin="Entry.id == foreign(entries_components.c.component_of_id)",
                                 backref=db.backref('component_ofs', lazy='dynamic'))

    def __repr__(self):
        return '<Entry %r - %r>'.format(self.english, self.german)

    def is_component_of(self, component_id):
        # TODO - this *maybe* doesn't work in edit view
        return component_id in self.component_ofs


class PosFeature(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    key = db.Column(db.Unicode)
    value = db.Column(db.Unicode)

    entry_id = db.Column(db.Integer, db.ForeignKey('entry.id'))
    entry = db.relationship("Entry", backref="pos_features")

    def __repr__(self):
        return '<PoS %r - %r of %r>'.format(self.key, self.value, self.entry)
