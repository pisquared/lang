import os
from datetime import datetime

import tts
from models import db, Entry, PosFeature, Group
from flask import Flask, render_template, request, redirect, url_for, \
  send_from_directory

app = Flask(__name__)


@app.route('/')
def index():
    entries = Entry.query.filter_by(variant_of=None).order_by(Entry.id.desc()).all()
    groups = Group.query.all()
    return render_template("index.html",
                           entries=entries,
                           groups=groups,
                           entry_types=Entry.ENTRY_TYPES,
                           pos_types=Entry.POS_TYPES,
                           )


@app.route('/entries/<int:entry_id>')
def get_entry(entry_id):
    entry = Entry.query.filter_by(id=entry_id).first()
    return render_template("entry.html",
                           entry=entry,
                           )


@app.route('/groups/<int:group_id>')
def get_group(group_id):
    group = Group.query.filter_by(id=group_id).first()
    return render_template("group.html",
                           group=group,
                           )


@app.route("/media/<path:filepath>")
def get_media(filepath):
    return send_from_directory('media', filepath)


@app.route('/create-entry', methods=["post"])
def create_entry():
    date = request.form.get("date")
    try:
        date = datetime.strptime(date, "%Y-%m-%d")
    except Exception:
        date = datetime.utcnow()
    english = request.form.get("english")
    german = request.form.get("german")
    entry_type = request.form.get("entry_type", "word")
    pos = request.form.get("pos")
    variant_of_id = request.form.get("variant_of")
    component_of_ids = request.form.get("component_of")
    group_ids = request.form.getlist("groups")
    new_group_name = request.form.get("new_group_name")

    entry = Entry(
        date=date,
        english=english,
        german=german,
        entry_type=entry_type,
        pos=pos,
        variant_of_id=variant_of_id if variant_of_id else None,
    )

    if component_of_ids:
        for component_of_id in component_of_ids:
            component_of_entry = Entry.query.filter_by(id=component_of_id).first()
            entry.components.append(component_of_entry)

    if group_ids:
        for group_id in group_ids:
            group = Group.query.filter_by(id=group_id).first()
            entry.groups.append(group)

    if new_group_name:
        group = Group(name=new_group_name)
        entry.groups.append(group)
        db.session.add(group)

    db.session.add(entry)

    # try to process Entry pos-kv
    pos_kv_input = request.form.get("pos_kv_input")
    pos_kv = pos_kv_input.split(",")
    try:
        for pos_feature in pos_kv:
            pos_k, pos_v = pos_feature.split(":")
            pos_feature = PosFeature(
                key=pos_k.strip(),
                value=pos_v.strip(),
                entry=entry,
            )
            db.session.add(pos_feature)
    except Exception as e:
        print(e)

    db.session.commit()

    # TODO: async
    en_filename = "{}_en.mp3".format(entry.id)
    en_filepath = os.path.join("media", en_filename)
    tts.gen_speech(entry.english, "en-US", en_filepath)
    entry.english_audio_filepath = en_filename

    de_filename = "{}_de.mp3".format(entry.id)
    de_filepath = os.path.join("media", de_filename)
    tts.gen_speech(entry.german, "de-de", de_filepath)
    entry.german_audio_filepath = de_filename

    db.session.add(entry)
    db.session.commit()
    return redirect(url_for("index"))


@app.route('/edit-entry/<int:entry_id>', methods=["get", "post"])
def edit_entry(entry_id):
    if request.method == "GET":
        entry = Entry.query.filter_by(id=entry_id).first()
        entries = Entry.query.filter_by(variant_of=None).order_by(Entry.id.desc()).all()
        groups = Group.query.all()
        return render_template("edit_entry.html",
                               entry=entry,
                               entries=entries,
                               groups=groups,
                               entry_types=Entry.ENTRY_TYPES,
                               pos_types=Entry.POS_TYPES,
                               )
    date = request.form.get("date")
    try:
        date = datetime.strptime(date, "%Y-%m-%d")
    except Exception:
        date = datetime.utcnow()
    english = request.form.get("english")
    german = request.form.get("german")
    entry_type = request.form.get("entry_type", "word")
    pos = request.form.get("pos")
    variant_of_id = request.form.get("variant_of")
    component_of_ids = request.form.get("component_of")
    group_ids = request.form.getlist("groups")
    new_group_name = request.form.get("new_group_name")

    entry = Entry(
        date=date,
        english=english,
        german=german,
        entry_type=entry_type,
        pos=pos,
        variant_of_id=variant_of_id if variant_of_id else None,
    )

    if component_of_ids:
        for component_of_id in component_of_ids:
            component_of_entry = Entry.query.filter_by(id=component_of_id).first()
            entry.components.append(component_of_entry)

    if group_ids:
        for group_id in group_ids:
            group = Group.query.filter_by(id=group_id).first()
            entry.groups.append(group)

    if new_group_name:
        group = Group(name=new_group_name)
        entry.groups.append(group)
        db.session.add(group)

    db.session.add(entry)

    # try to process Entry pos-kv
    pos_kv_input = request.form.get("pos_kv_input")
    pos_kv = pos_kv_input.split(",")
    try:
        for pos_feature in pos_kv:
            pos_k, pos_v = pos_feature.split(":")
            pos_feature = PosFeature(
                key=pos_k.strip(),
                value=pos_v.strip(),
                entry=entry,
            )
            db.session.add(pos_feature)
    except Exception as e:
        print(e)

    db.session.commit()

    # TODO: async
    en_filename = "{}_en.mp3".format(entry.id)
    en_filepath = os.path.join("media", en_filename)
    tts.gen_speech(entry.english, "en-US", en_filepath)
    entry.english_audio_filepath = en_filename

    de_filename = "{}_de.mp3".format(entry.id)
    de_filepath = os.path.join("media", de_filename)
    tts.gen_speech(entry.german, "de-de", de_filepath)
    entry.german_audio_filepath = de_filename

    db.session.add(entry)
    db.session.commit()
    return redirect(url_for("index"))


if __name__ == '__main__':
    basepath = os.path.dirname(os.path.realpath(__file__))

    dbfile = os.path.join(basepath, "app.db")
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///{dbfile}'.format(dbfile=dbfile)

    db.init_app(app)
    with app.app_context():
        if not os.path.isfile(dbfile):
            db.create_all()

            w1 = Entry(
                date=datetime.utcnow(),
                english="question",
                german="Frage",
                entry_type=Entry.ENTRY_TYPE_WORD,
                pos=Entry.POS_TYPE_NOUN,
                english_audio_filepath="14.wav",
                german_audio_filepath="14.wav",
            )

            w1_pos_1 = PosFeature(
                key="number",
                value="singular",
                entry=w1,
            )

            w2 = Entry(
                date=datetime.utcnow(),
                english="questions",
                german="Fragen",
                variant_of=w1,
                entry_type=Entry.ENTRY_TYPE_WORD,
                pos=Entry.POS_TYPE_NOUN,
            )

            w2_pos_1 = PosFeature(
                key="number",
                value="plural",
                entry=w2,
            )

            db.session.add(w1)
            db.session.commit()

            w1_english_audio_filename = "{}_en.mp3".format(w1.id)
            w1_german_audio_filename = "{}_de.mp3".format(w1.id)
            w2_english_audio_filename = "{}_en.mp3".format(w2.id)
            w2_german_audio_filename = "{}_de.mp3".format(w2.id)
            w1_english_audio_filepath = os.path.join("media", w1_english_audio_filename)
            w1_german_audio_filepath = os.path.join("media", w1_german_audio_filename)
            w2_english_audio_filepath = os.path.join("media", w2_english_audio_filename)
            w2_german_audio_filepath = os.path.join("media", w2_german_audio_filename)

            if not os.path.isfile(w1_english_audio_filepath):
                tts.gen_speech(w1.english, "en-US", w1_english_audio_filepath)
            if not os.path.isfile(w1_german_audio_filepath):
                tts.gen_speech(w1.german, "de-de", w1_german_audio_filepath)
            if not os.path.isfile(w2_english_audio_filepath):
                tts.gen_speech(w2.english, "en-US", w2_english_audio_filepath)
            if not os.path.isfile(w2_german_audio_filepath):
                tts.gen_speech(w2.german, "de-de", w2_german_audio_filepath)

            w1.english_audio_filepath = w1_english_audio_filename
            w1.german_audio_filepath = w1_german_audio_filename
            w2.english_audio_filepath = w2_english_audio_filename
            w2.german_audio_filepath = w2_german_audio_filename
            db.session.add(w1)
            db.session.add(w2)
            db.session.commit()

    @app.template_filter('format_dt')
    def format_datetime(dt, formatting="%A, %d %b %Y"):
        """
        Formats the datetime string provided in value into whatever format you want that is supported by python strftime
        http://strftime.org/
        :param formatting The specific format of the datetime
        :param dt a datetime object
        :return:
        """
        return dt.strftime(formatting)


    app.run(debug=True)
